<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{


    public $timestamps = false;
    protected $fillable = ['id', 'date', 'description'];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }
}
