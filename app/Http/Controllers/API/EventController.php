<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return $events;
    }
    public function destroy($id){
        $event = Event::find($id);
        if (!$event) {
            return respones()->json(['message' => 'Record not found'], 404);
        }
        $event-> delete();
        return['deleted' => $id];
    }

    public function show($id){
        $event = Event::find($id);
        if ($event) {
            return $event;
        } else {
            return response()->json(['message' => 'record not found'], 404);
        }
    }
    public function update($id){
        $event = Event::find($id);
        $event->fill($request->all());
        $event->save();
        return['updated' => $id];
    }
}
