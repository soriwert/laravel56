<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Group;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();
        return $groups;
    }
    public function destroy($id){
        $group = Group::find($id);
        if (!$group) {
            return respones()->json(['message' => 'Record not found'], 404);
        }
        $group-> delete();
        return['deleted' => $id];
    }

    public function show($id){
        $group = Group::find($id);
        if ($group) {
            return $group;
        } else {
            return response()->json(['message' => 'record not found'], 404);
        }
    }
    public function update($id){
        $group = Group::find($id);
        $group->fill($request->all());
        $group->save();
        return['updated' => $id];
    }
}
