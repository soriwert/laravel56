<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Group;

class EventController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::paginate();
        return view('event.event', ['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Event::class);
        $validatedData = $request->validate(['date' => 'date_format:"Y-m-d"|required']);
        $event = new Event();
        $event->fill($request->all());
        $event->save();
        return redirect('/events');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        $this->authorize('view', $event);
        return view('event.show', ['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        $this->authorize('update', $event);
        $groups = Group::all();
        return view('event.edit', ['event' => $event, 'groups'=>$groups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
        'date' => 'date_format:"Y-m-d"|required'
        ]);
        $event = Event::find($id);
        $this->authorize('update', $event);
        $event->description = $request->input('description');
        $event->save();
        return redirect('/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $this->authorize('delete', Event::class);
        $event->delete();
        return redirect('/events');
    }
    public function addGroup(Request $request, $id){

        $group= $request->input('group');
        $event = Event::find($id);
        // $event->groups()->atach($group);
        // $event->groups()->sync($group);
        $event->groups()->syncWithoutDetaching($group);
        return back();
    }
    public function deleteGroup(Request $request, $id){
        $event = Event::find($id);
        // $group= $request->input('group');
        // dd($group);
        $group = $request->input('optradio');
        $event->groups()->detach($group);
        return back();

    }
}
