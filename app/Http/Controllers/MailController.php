<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\EventSend;
use App\Event;
use Mail;

class MailController extends Controller
{
    public function mail(Request $request, $eventId)
    {
        $event = Event::find($eventId);
        Mail::to($request->user())->send(new EventSend($event));
        return 'correo enviado';
    }
}
