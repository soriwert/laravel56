<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $timestamps = false;
    protected $fillable = ['group'];

    public function groups()
    {
        return $this->hasMany('App\User');
    }

    public function events()
    {
        return $this->belongsTo('App\Event');
    }
}
