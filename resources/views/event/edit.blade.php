@extends('layouts.app')
@section('content')
<h1>Edición de evento</h1>

<form method="post" action="/events/{{ $event->id }}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
    <div class="form-group">
        <label>Id</label>
        <input type="text" name="id" class="form-control" value="{{ $event->id }}" disabled="true">
    </div>

    <div class="form-group">
        <label>date</label>
        <input type="text" name="date" class="form-control" value="{{ $event->date }}">
    </div>

    <div class="form-group">
        <label>description</label>
        <input type="text" name="description" class="form-control" value="{{ $event->description }}">
    </div>

    <div class="form-group">
        <input type="submit" name="" value="Editar" class="form-control">
    </div>

</form>
<div class="row">

    <div class="col-md-6">

        <form method="post" action="/events/{{ $event->id }}/groups">
            {{ csrf_field() }}
            <label> Selección </label>
            <select name="group">
                @foreach($groups as $group)
                <option value="{{$group->id}}">{{$group->group}}</option>
                @endforeach
            </select>
            <input type="submit" name="Enviar">
        </form>
    </div>
    <div class="col-md-6">
        <form method="post" action="/events/{{ $event->id }}/groups">
            <input type="hidden" name="_method" value="delete">
            {{ csrf_field() }}
            @foreach ($event->groups as $group)
            <div class="radio">
                <label><input type="radio" name="optradio" value="{{$group->id}}">{{ $group->group }}</label>
            </div>
            @endforeach
            <input type="submit" value="Borrar">
        </form>
    </div>
</div>


@endsection
