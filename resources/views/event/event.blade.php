@extends('layouts.app')

@section('content')
<h1>Lista de eventos</h1>

<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>fecha</th>
        <th>Descripcion</th>
        <th>Acciones</th>
    </tr>
    @foreach($events as $event)
    <tr>
        <td>{{$event->id}}</td>
        <td>{{$event->date}}</td>
        <td>{{$event->description}}</td>
        <td>
            <a href="/events/{{$event->id}}">Ver</a>
            @can ('view',$event)
            <a href="/events/{{$event->id}}/edit">Editar</a>
            <a href="/event/{{$event->id}}/mail">
                <img class="img" src="images/mail.png" width="3.33%" height="auto" style="margin-left: 2px">
            </a>
            <form method="post" action="/events/{{$event->id}}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar">
            </form>

            @endcan
        </td>
    </tr>
    @endforeach
</table>
<a href="/events/create">Nuevo</a>
{{ $events->links() }}

@endsection
