@extends('layouts.app')

@section('content')
<h1>Lista de usuarios</h1>

<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>Nombre</th>
        <th>email</th>
        <th>role</th>
        <th>acciones</th>
    </tr>
    @foreach($users as $user)
    <tr>
        <td>{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->role->name}}</td>
        <td></td>
    </tr>
    @endforeach
</table>

@endsection
