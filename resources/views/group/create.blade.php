@extends('layouts.app')

@section('content')
<h1>Crear grupo</h1>
<form method="post" action="/groups">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Grupo</label>
        <input type="text" name="group">
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div><input type="submit" name="enviar"></div>
</form>


@endsection
