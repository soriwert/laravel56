@extends('layouts.app')

@section('content')
<h1>Lista de usuarios</h1>

<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>Grupos</th>
        <th>Acciones</th>
    </tr>
    @foreach($groups as $group)
    <tr>
        <td>{{$group->id}}</td>
        <td>{{$group->group}}</td>
        <td>
            <a href="/groups/{{$group->id}}">Ver</a>
            @can ('view', $group)
            <a href="/groups/{{$group->id}}/edit">Editar</a>
            <form method="post" action="/groups/{{$group->id}}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar">
            </form>
            @endcan
        </td>
    </tr>
    @endforeach
</table>
<a href="/groups/create">Nuevo</a>


{{ $groups->links() }}

@endsection
