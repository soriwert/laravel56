@extends('layouts.app')
@section('content')
<h1>Edición de grupo</h1>

<form method="post" action="/groups/{{ $group->id }}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
    <div class="form-group">
        <label>Id</label>
        <input type="text" name="id" class="form-control" value="{{ $group->id }}" disabled="true">
    </div>

    <div class="form-group">
        <label>Grupo</label>
        <input type="text" name="group" class="form-control" value="{{ $group->group }}">
    </div>

    <div class="form-group">
        <input type="submit" name="" value="Editar" class="form-control">
    </div>
</form>

@endsection
