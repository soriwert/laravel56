@extends('layouts.app')

@section('content')
<h1>Lista de artículos</h1>

<table class="table table-bordered">
    <tr>
        <th>id</th>
        <th>Nombre</th>
        <th>acciones</th>
    </tr>
    @foreach($roles as $role)
    <tr>
        <td>{{$role->id}}</td>
        <td>{{$role->name}}</td>
        <td></td>
    </tr>
    @endforeach
</table>

@endsection
