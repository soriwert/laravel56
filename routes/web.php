<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function ()
{
    return view('welcome');
});

Route::get('/roles', 'RoleController@index');
Route::get('/users', 'UserController@index');
Route::resource('/events', 'EventController');
Route::post('/events/{id}/groups', 'EventController@addGroup');
Route::resource('/groups', 'GroupController');
Route::delete('/events/{id}/groups', 'EventController@deleteGroup');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/redirect/google', 'SocialAuthController@redirect');
Route::get('/callback/google', 'SocialAuthController@callback');

Route::get("/event/{id}/mail", 'MailController@mail');
