<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/roles', 'API\RoleController@index');
Route::get('/users', 'API\UserController@index');
Route::resource('/events', 'API\EventController', ['except' => ['create','edit']]);
Route::resource('/groups', 'API\GroupController', ['except' => ['create','edit']]);
