@extends('layouts.app')

@section('content')
<h1>Crear evento</h1>
<form method="post" action="/events">
    {{ csrf_field() }}
    <div class="form-group">
        <label>date</label>
        <input type="text" name="date">
        <label>description</label>
        <input type="text" name="description">
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div><input type="submit" name="enviar"></div>
</form>

@endsection
