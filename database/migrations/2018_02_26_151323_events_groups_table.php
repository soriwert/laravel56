<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventsGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_group', function (Blueprint $table) {
            $table->integer('event_Id')->unsigned();
            $table->foreign('event_Id')->references('id')->on('events');
            $table->integer('group_Id')->unsigned();
            $table->foreign('group_Id')->references('id')->on('groups')->onDelete('cascade');
            $table->primary(['event_Id', 'group_Id']);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_group');
    }
}
