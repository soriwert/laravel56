<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'date' => date('y.m.d'),
            'description' => "evento1"
        ]);
        DB::table('events')->insert([
            'date' => date('y.m.d'),
            'description' => "evento2"
        ]);
    }
}

