<?php

use Illuminate\Database\Seeder;

class InsertUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "patricio",
            'email' => "admin@gmail.com",
            'password' => bcrypt('secret'),
            'role_id' => 1,
        ]);
        DB::table('users')->insert([
            'name' => "Alex",
            'email' => "user@gmail.com",
            'password' => bcrypt('secret'),
            'role_id' => 2,
        ]);

    }
}
