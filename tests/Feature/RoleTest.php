<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    /** @test */
    public function testIndex()
    {
        $this->get('/roles')
            ->assertStatus(200)
            ->assertSee('Laravel')
            ->assertSee('administrador')
            ->assertSee('usuario');

    }
}
