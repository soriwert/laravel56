<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class APITest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUsers()
    {
        $response = $this->json('GET', '/api/users');
        $response
        ->assertStatus(200)
        ->assertExactJson([
            [
                "id"=> 4,
                "name"=> "Alex",
                "email"=> "alex@gmail.com",
                "created_at"=> null,
                "updated_at"=> null,
                "role_id"=> 2
            ],
            [
                "id"=> 1,
                "name"=> "patricio",
                "email"=> "soriwert@gmail.com",
                "created_at"=> null,
                "updated_at"=> null,
                "role_id"=> 1
        ]]);
    }

    public function testRoles()
    {
        $response = $this->json('GET', '/api/roles');
        $response
        ->assertStatus(200)
        ->assertExactJson([
            ["id"=>1,"name"=>"administrador","created_at"=>null,"updated_at"=>null],
            ["id"=>2,"name"=>"usuario","created_at"=>null,"updated_at"=>null]
        ]);
    }
}
